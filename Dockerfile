# Use the official Fedora 33 Python 3 image
FROM registry.fedoraproject.org/f33/python3

# Set the working directory in the container
WORKDIR /app

# Copy the script and configuration files into the container
COPY certificate_expiry_checker.py .
COPY config.json .

# Copy the requirements.txt file into the container
COPY requirements.txt .

# Install the required Python packages from the requirements.txt file
RUN pip install --no-cache-dir -r requirements.txt

# Expose any necessary ports
# EXPOSE 8080

# Set environment variables (if needed)
# ENV CERTIFICATE_EXPIRATION_THRESHOLD_DAYS 90

# Run the script when the container starts
CMD ["python", "certificate_expiry_checker.py"]
